const fs = require('fs');
const crypto = require('crypto');

let serviceUserFile = fs.readFileSync('service_user.json');
let b24AccountsFile = fs.readFileSync('b24_accounts.json');

let serviceUsers = JSON.parse(serviceUserFile);
let b24Accounts = JSON.parse(b24AccountsFile);
serviceUsers = serviceUsers[2].data;
b24Accounts = b24Accounts[0].subaccounts;

// console.log('this is serviceUsers: ', serviceUsers);
// console.log('this is b24Accounts: ', b24Accounts);

let id = crypto.randomBytes(32).toString('hex');
const whitelist = '54.229.22.212 52.208.195.248';


console.log('found account: ' + findAccount(serviceUsers, '7576').id);

function findAccount(accounts, id) {
  return accounts.find( (account) => {
    return account.owner_id === id;
  });
}

function filterAccounts(accounts) {
  let filteredAccounts = [];

  for(let account in accounts) {

    if(!findAccount(serviceUsers,accounts[account].id)) {
      filteredAccounts.push({
        id: accounts[account].id,
        username: accounts[account].username,
        apiKey: crypto.randomBytes(32).toString('hex'),
        whitelist: whitelist
      });
    }
  }
  return filteredAccounts;
}

function writeToFile(object) {
  fs.writeFileSync('./data.json', JSON.stringify(object, null, 2) , 'utf-8');
}


const result = filterAccounts(b24Accounts);
writeToFile(result);
console.log('this is our result: ', result);
console.log('this is the length of our result: ', result.length);

console.log('length of service_users and beds users combined: ', (serviceUsers.length + result.length));
console.log('length of total users: ', Object.keys(b24Accounts).length);
